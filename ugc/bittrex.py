import requests

from logging import getLogger


logger = getLogger(__name__)

class BittrexError(Exception):
    """
    Unknown error while requesting the Bittrex API
    """


class BittrexRequestError(BittrexError):
    """
    Error on invalid request
    """


class BittrexClient(object):
    """
    Client to API Bittrex
    Help on: https://bittrex.github.io/api/v3
    """
    def __init__(self):
        self.base_url = "https://api.bittrex.com/v3"

    def __request(self, method, params=None):
        url = self.base_url + method + params + "/summary"
        try:
            r = requests.get(url=url)
            result =  r.json()
        except Exception:
            logger.exception("Bittrex error")
            raise BittrexError

        if result.get("high"):
            # Successful request
            return result["high"]
        else:
            # Invalid request
            logger.error("Request error: %s", result.get("message"))
            raise BittrexRequestError

    def get_last_price(self, pair):
        res = self.__request(method="/markets/", params=pair)
        return res

