import functools
import datetime

from django.core.management.base import BaseCommand
from django.conf import settings

from logging import getLogger

from subprocess import Popen
from subprocess import PIPE

from telegram import Bot
from telegram import Update
from telegram import ParseMode
from telegram import InlineKeyboardButton
from telegram import InlineKeyboardMarkup
from telegram import ReplyKeyboardRemove
from telegram.ext import CallbackContext
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram.ext import CallbackQueryHandler
from telegram.utils.request import Request

from telegram import KeyboardButton
from telegram import ReplyKeyboardMarkup

from ugc.models import Message
from ugc.models import Profile

from ugc.bittrex import BittrexClient
from ugc.bittrex import BittrexError

from ugc.horoscope import HoroscopAPI
from ugc.horoscope import HoroscopError


client = BittrexClient()
zodiac_sing = HoroscopAPI()

BUTTON1_HELP = "Help"
BUTTON2_TIME = "Time"
BUTTON3_COUNT = "Number of messages"


def get_base_reply_keyboard():
    keyboard = [
        [
            KeyboardButton(BUTTON3_COUNT),
        ],
        [
            KeyboardButton(BUTTON1_HELP),
            KeyboardButton(BUTTON2_TIME),
        ],
    ]
    return ReplyKeyboardMarkup(
        keyboard=keyboard,
        resize_keyboard=True,
    )


def logger_factory(logger):
    """
    The function is imported before loading the logging config.
    It is necessary to explicitly indicate which logger we want to write to.
    """
    def debug_requests(f):

        @functools.wraps(f)
        def inner(*args, **kwargs):

            try:
                logger.debug('Treatment in function `{}`'.format(f.__name__))
                return f(*args, **kwargs)
            except Exception as e:
                logger.exception('Error in function `{}`'.format(f.__name__))
                # sentry_sdk.capture_exception(error=e)
                raise

        return inner

    return debug_requests


logger = getLogger(__name__)

debug_requests = logger_factory(logger=logger)


# `callback_data` -- will send a TG when each button is pressed.
# Each identifier must be unique.

CALLBACK_BUTTON1_LEFT = "callback_button1_left"
CALLBACK_BUTTON2_RIGHT = "callback_button2_right"
CALLBACK_BUTTON3_MORE = "callback_button3_more"
CALLBACK_BUTTON4_BACK = "callback_button4_back"
CALLBACK_BUTTON4_1_BACK = "callback_button4_1_back"
CALLBACK_BUTTON5_TIME = "callback_button5_time"
CALLBACK_BUTTON6_PRICE = "callback_button6_price"
CALLBACK_BUTTON7_PRICE = "callback_button7_price"
CALLBACK_BUTTON8_PRICE = "callback_button8_price"
CALLBACK_BUTTON9_HOROSCOP = "callback_button_horoscop"
CALLBACK_BUTTON10_ARIES = "callback_button_aries"
CALLBACK_BUTTON11_TAURUS = "callback_button_taurus"
CALLBACK_BUTTON12_GEMINI = "callback_button_gemini"
CALLBACK_BUTTON13_CANCER = "callback_button_cancer"
CALLBACK_BUTTON14_LEO = "callback_button_leo"
CALLBACK_BUTTON15_VIRGO = "callback_button_virgo"
CALLBACK_BUTTON16_LIBRA = "callback_button_libra"
CALLBACK_BUTTON17_SCORPIO = "callback_button_scorpio"
CALLBACK_BUTTON18_SAGITTARIUS = "callback_button_sagittarius"
CALLBACK_BUTTON19_CAPRICORN = "callback_button_capricorn"
CALLBACK_BUTTON20_AQUARIUS = "callback_button_aquarius"
CALLBACK_BUTTON21_PISCES = "callback_button_pisces"
CALLBACK_BUTTON22_TODAY = "callback_button_today"
CALLBACK_BUTTON23_WEEK = "callback_button_week"
CALLBACK_BUTTON24_MONTH = "callback_button_month"
CALLBACK_BUTTON25_YEAR = "callback_button_year"
CALLBACK_BUTTON_HIDE_KEYBOARD = "callback_button9_hide"


TITLES = {
    CALLBACK_BUTTON1_LEFT: "New messages ⚡️",
    CALLBACK_BUTTON2_RIGHT: "Edit ✏️",
    CALLBACK_BUTTON3_MORE: "BTC rate ➡️",
    CALLBACK_BUTTON4_BACK: "Back ⬅️",
    CALLBACK_BUTTON4_1_BACK: "Back ⬅️",
    CALLBACK_BUTTON5_TIME: "Time ⏰",
    CALLBACK_BUTTON6_PRICE: "USD 💰",
    CALLBACK_BUTTON7_PRICE: "EUR 💰",
    CALLBACK_BUTTON8_PRICE: "USDT 💰",
    CALLBACK_BUTTON9_HOROSCOP: "Horoscop",
    CALLBACK_BUTTON10_ARIES: "Aries",
    CALLBACK_BUTTON11_TAURUS: "Taurus",
    CALLBACK_BUTTON12_GEMINI: "Gemini",
    CALLBACK_BUTTON13_CANCER: "Cancer",
    CALLBACK_BUTTON14_LEO: "Leo",
    CALLBACK_BUTTON15_VIRGO: "Virgo",
    CALLBACK_BUTTON16_LIBRA: "Libra",
    CALLBACK_BUTTON17_SCORPIO: "Scorpio",
    CALLBACK_BUTTON18_SAGITTARIUS: "Sagittarius",
    CALLBACK_BUTTON19_CAPRICORN: "Capricorn",
    CALLBACK_BUTTON20_AQUARIUS: "Aquarius",
    CALLBACK_BUTTON21_PISCES: "Pisces",
    CALLBACK_BUTTON22_TODAY: "Today",
    CALLBACK_BUTTON23_WEEK: "Week",
    CALLBACK_BUTTON24_MONTH: "Month",
    CALLBACK_BUTTON25_YEAR: "Year",
    CALLBACK_BUTTON_HIDE_KEYBOARD: "Hide keyboard",
}


def get_base_inline_keyboard():
    """
    Get keyboard for message.
    The keyboard will be visible under each message where it was attached.
    """

    # Each list inside `keyboard` is one horizontal row of buttons.
    keyboard = [
        # Each item inside the list is one vertical column.
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON1_LEFT], callback_data=CALLBACK_BUTTON1_LEFT),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON2_RIGHT], callback_data=CALLBACK_BUTTON2_RIGHT),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON9_HOROSCOP], callback_data=CALLBACK_BUTTON9_HOROSCOP),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON3_MORE], callback_data=CALLBACK_BUTTON3_MORE),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON_HIDE_KEYBOARD], callback_data=CALLBACK_BUTTON_HIDE_KEYBOARD),
        ],
    ]
    return InlineKeyboardMarkup(keyboard)


def get_keyboard2():
    """
    Get second keyboard page for messages Possible to get only by pressing
    a button on the first keyboard.
    """
    keyboard = [
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON5_TIME], callback_data=CALLBACK_BUTTON5_TIME),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON6_PRICE], callback_data=CALLBACK_BUTTON6_PRICE),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON7_PRICE], callback_data=CALLBACK_BUTTON7_PRICE),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON8_PRICE], callback_data=CALLBACK_BUTTON8_PRICE),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON4_BACK], callback_data=CALLBACK_BUTTON4_BACK),
        ],
    ]
    return InlineKeyboardMarkup(keyboard)

def get_keyboard3():
    """
    Get third keyboard page for messages Possible to get only by pressing
    a button on the first keyboard.
    """
    keyboard = [
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON10_ARIES], callback_data=CALLBACK_BUTTON10_ARIES),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON11_TAURUS], callback_data=CALLBACK_BUTTON11_TAURUS),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON12_GEMINI], callback_data=CALLBACK_BUTTON12_GEMINI),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON13_CANCER], callback_data=CALLBACK_BUTTON13_CANCER),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON14_LEO], callback_data=CALLBACK_BUTTON14_LEO),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON15_VIRGO], callback_data=CALLBACK_BUTTON15_VIRGO),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON16_LIBRA], callback_data=CALLBACK_BUTTON16_LIBRA),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON17_SCORPIO], callback_data=CALLBACK_BUTTON17_SCORPIO),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON18_SAGITTARIUS], callback_data=CALLBACK_BUTTON18_SAGITTARIUS),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON19_CAPRICORN], callback_data=CALLBACK_BUTTON19_CAPRICORN),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON20_AQUARIUS], callback_data=CALLBACK_BUTTON20_AQUARIUS),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON21_PISCES], callback_data=CALLBACK_BUTTON21_PISCES),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON4_BACK], callback_data=CALLBACK_BUTTON4_BACK),
        ],
    ]

    return InlineKeyboardMarkup(keyboard)

def get_keyboard4(sing=""):
    """
    Get third keyboard page for messages Possible to get only by pressing
    a button on the first keyboard.
    """
    keyboard = [
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON22_TODAY], callback_data=str(CALLBACK_BUTTON22_TODAY+"|"+sing)),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON23_WEEK], callback_data=str(CALLBACK_BUTTON23_WEEK+"|"+sing)),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON24_MONTH], callback_data=str(CALLBACK_BUTTON24_MONTH+"|"+sing)),
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON25_YEAR], callback_data=str(CALLBACK_BUTTON25_YEAR+"|"+sing)),
        ],
        [
            InlineKeyboardButton(TITLES[CALLBACK_BUTTON4_1_BACK], callback_data=CALLBACK_BUTTON4_1_BACK),
        ],
    ]
    return InlineKeyboardMarkup(keyboard)


@debug_requests
def keyboard_callback_handler(update: Update, context: CallbackContext):
    """
    Handler for ALL buttons from ALL keyboards.
    """
    query = update.callback_query
    data = query.data
    now = datetime.datetime.now()

    # Note: `effective_message` is used.
    chat_id = update.effective_message.chat_id
    current_text = update.effective_message.text

    if data == CALLBACK_BUTTON1_LEFT:
        # Let's "remove" the keyboard from the previous message.
        query.edit_message_text(
            text=current_text,
            parse_mode=ParseMode.MARKDOWN,
        )
        # Let's send a new message when clicking on the button.
        context.bot.send_message(
            chat_id=chat_id,
            text="New massage\n\ncallback_query.data={}".format(data),
            reply_markup=get_base_inline_keyboard(),
        )
    elif data == CALLBACK_BUTTON2_RIGHT:
        # Let's edit the text of the message, but leave the keyboard.
        query.edit_message_text(
            text="Done in {}".format(now),
            reply_markup=get_base_inline_keyboard(),
        )
    elif data == CALLBACK_BUTTON3_MORE:
        # Show next keyboard screen,
        # (keep the same text, but specify a different array of buttons).
        query.edit_message_text(
            text=current_text,
            reply_markup=get_keyboard2(),
        )
    elif data == CALLBACK_BUTTON9_HOROSCOP:
        # Show next keyboard screen,
        # (keep the same text, but specify a different array of buttons).
        query.edit_message_text(
            text="Would you choose the horoscope sign, please",
            reply_markup=get_keyboard3(),
        )
    elif data in {CALLBACK_BUTTON10_ARIES, CALLBACK_BUTTON11_TAURUS, CALLBACK_BUTTON12_GEMINI,
                  CALLBACK_BUTTON13_CANCER, CALLBACK_BUTTON14_LEO, CALLBACK_BUTTON15_VIRGO,
                  CALLBACK_BUTTON16_LIBRA, CALLBACK_BUTTON17_SCORPIO, CALLBACK_BUTTON18_SAGITTARIUS,
                  CALLBACK_BUTTON19_CAPRICORN, CALLBACK_BUTTON20_AQUARIUS, CALLBACK_BUTTON21_PISCES}:
        # Show next keyboard screen,
        # (keep the same text, but specify a different array of buttons).
        sing = str(query.data)
        query.edit_message_text(
            text="Would you choose period, please",
            reply_markup=get_keyboard4(sing),
        )
    elif data == CALLBACK_BUTTON4_BACK:
        # Show previous keyboard screen,
        # (keep the same text, but specify a different array of buttons).
        query.edit_message_text(
            text=current_text,
            reply_markup=get_base_inline_keyboard(),
        )
    elif data == CALLBACK_BUTTON4_1_BACK:
        # Show previous keyboard screen,
        # (keep the same text, but specify a different array of buttons).
        query.edit_message_text(
            text="Would you choose the horoscope sign, please",
            reply_markup=get_keyboard3(),
        )
    elif data == CALLBACK_BUTTON5_TIME:
        # Let's show the new text and keep the same keyboard.
        text = "*Precise time*\n\n{}".format(now)
        query.edit_message_text(
            text=text,
            parse_mode=ParseMode.MARKDOWN,
            reply_markup=get_keyboard2(),
        )
    elif data in (CALLBACK_BUTTON6_PRICE, CALLBACK_BUTTON7_PRICE, CALLBACK_BUTTON8_PRICE):
        pair = {
            CALLBACK_BUTTON6_PRICE: "BTC-USD",
            CALLBACK_BUTTON7_PRICE: "BTC-EUR",
            CALLBACK_BUTTON8_PRICE: "BTC-USDT",
        }[data]

        try:
            current_price = client.get_last_price(pair=pair)
            text = "*Exchange rate BTC to:*\n\n*{}* = {}$".format(pair, current_price)
        except BittrexError:
            text = "An error occurred :(\n\nCould you repeat, please"
        query.edit_message_text(
            text=text,
            parse_mode=ParseMode.MARKDOWN,
            reply_markup=get_keyboard2(),
        )


    # elif data in (CALLBACK_BUTTON10_ARIES, CALLBACK_BUTTON11_TAURUS, CALLBACK_BUTTON12_GEMINI,
    #               CALLBACK_BUTTON13_CANCER, CALLBACK_BUTTON14_LEO, CALLBACK_BUTTON15_VIRGO,
    #               CALLBACK_BUTTON16_LIBRA, CALLBACK_BUTTON17_SCORPIO, CALLBACK_BUTTON18_SAGITTARIUS,
    #               CALLBACK_BUTTON19_CAPRICORN, CALLBACK_BUTTON20_AQUARIUS, CALLBACK_BUTTON21_PISCES):
    #     sing = {
    #         CALLBACK_BUTTON10_ARIES: "Aries",
    #         CALLBACK_BUTTON11_TAURUS: "Taurus",
    #         CALLBACK_BUTTON12_GEMINI: "Gemini",
    #         CALLBACK_BUTTON13_CANCER: "Cancer",
    #         CALLBACK_BUTTON14_LEO: "Leo",
    #         CALLBACK_BUTTON15_VIRGO: "Virgo",
    #         CALLBACK_BUTTON16_LIBRA: "Libra",
    #         CALLBACK_BUTTON17_SCORPIO: "Scorpio",
    #         CALLBACK_BUTTON18_SAGITTARIUS: "Sagittarius",
    #         CALLBACK_BUTTON19_CAPRICORN: "Capricorn",
    #         CALLBACK_BUTTON20_AQUARIUS: "Aquarius",
    #         CALLBACK_BUTTON21_PISCES: "Pisces",
    #     }[data]
    #
    #     try:
    #         horoscop_on = zodiac_sing.get_last_text(period=period, sing=libra)
    #         text = "*Horoscop to:*\n*{}*\n\n{}$".format(period, horoscop_on)
    #     except HoroscopError:
    #         text = "An error occurred :(\n\nCould you repeat, please"
    #     query.edit_message_text(
    #         text=text,
    #         parse_mode=ParseMode.MARKDOWN,
    #         reply_markup=get_keyboard3(),
    #     )


    elif data.split('|')[0] in (CALLBACK_BUTTON22_TODAY, CALLBACK_BUTTON23_WEEK,
                  CALLBACK_BUTTON24_MONTH, CALLBACK_BUTTON25_YEAR):
        period = {
            CALLBACK_BUTTON22_TODAY: "today",
            CALLBACK_BUTTON23_WEEK: "week",
            CALLBACK_BUTTON24_MONTH: "month",
            CALLBACK_BUTTON25_YEAR: "year",
        }[data.split('|')[0]]
        sing = data.split('|')[1].split("_")[-1]
        horoscope = zodiac_sing.get_last_text(period=period, sing=sing)
        if period == "today":
            date_horoscope = horoscope["date"]
        else:
            date_horoscope = horoscope[period]
        try:
            text = "*Horoscop to {}:*\n*{}* - *{}*\n\n{}".format(horoscope['sunsign'],
                                                                 period,
                                                                 date_horoscope,
                                                                 horoscope['horoscope'])
        except HoroscopError:
            text = "An error occurred :(\n\nCould you repeat, please"
        query.edit_message_text(
            text=text,
            parse_mode=ParseMode.MARKDOWN,
            reply_markup=get_keyboard4(sing),
        )


    elif data == CALLBACK_BUTTON_HIDE_KEYBOARD:
        # Hide keyboard.
        # Only works when sending a new message.
        # It could be edited, but then you need to know for sure that
        # the message did not have buttons.
        context.bot.send_message(
            chat_id=chat_id,
            text="You hide keyboard\n\nPush /start back",
            reply_markup=ReplyKeyboardRemove(),
        )


@debug_requests
def do_count(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id

    p, _ = Profile.objects.get_or_create(
        external_id=chat_id,
        defaults={
            'name': update.message.from_user.username,
        }
    )
    count = Message.objects.filter(profile=p).count()
    update.message.reply_text(
        text = f'\nYou have {count} massage(s)',
    )


@debug_requests
def do_start(update: Update, context: CallbackContext):
    update.message.reply_text(
        text="Hi! Send me something",
        reply_markup=get_base_reply_keyboard(),
    )


@debug_requests
def do_help(update: Update, context: CallbackContext):
    update.message.reply_text(
        text="It is a test bot\n\n"
             "You could try it \n\n"
             "I will answer all questions ASAP",
        reply_markup=get_base_inline_keyboard(),
    )


@debug_requests
def do_time(update: Update, context: CallbackContext):
    """
    Find out server time.
    """
    process = Popen(["date"], stdout=PIPE)
    text, error = process.communicate()
    # Process call error may occur (return code not 0).
    if error:
        text = "You have error, time is unknown"
    else:
        # Decode command response from process.
        text = text.decode("utf-8")
    update.message.reply_text(
        text=text,
        reply_markup=get_base_inline_keyboard(),
    )


def log_errors(f):

    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            error_message = f'You have error: {e}'
            print(error_message)
            raise e
    return inner


@debug_requests
def do_echo(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    text = update.message.text
    if text == BUTTON1_HELP:
        return do_help(update=update, context=context)
    elif text == BUTTON2_TIME:
        return do_time(update=update, context=context)
    elif text == BUTTON3_COUNT:
        return do_count(update=update, context=context)
    else:
        reply_text = "You ID = {}\n\n{}".format(chat_id, text)
        update.message.reply_text(
            text=reply_text,
            reply_markup=get_base_inline_keyboard(),
        )

    chat_id = update.message.chat_id
    text = update.message.text

    p, _ = Profile.objects.get_or_create(
        external_id=chat_id,
        defaults={
            'name': update.message.from_user.username,
        }
    )
    m = Message(
        profile=p,
        text=text,
    )
    m.save()

    # reply_text = f'Ваш ID = {chat_id}\n{text}'
    # update.message.reply_text(
    #     text=reply_text,
    # )


class Command(BaseCommand):
    help = 'Telegram bot'

    def handle(self, *args, **options):
        # 1 - correct connection
        request = Request(
            connect_timeout=0.5,
            read_timeout=1.0,
        )
        bot = Bot(
            request=request,
            token=settings.TOKEN,
            # base_url=settings.PROXY_URL,
        )
        print(bot.get_me())

        # 2 - handlers
        updater = Updater(
            bot=bot,
            use_context=True,
        )

        # Check that the bot has correctly connected to the Telegram API.
        info = bot.get_me()
        logger.info(f'Bot info: {info}')

        start_handler = CommandHandler("start", do_start)
        help_handler = CommandHandler("help", do_help)
        buttons_handler = CallbackQueryHandler(callback=keyboard_callback_handler)
        message_handler = MessageHandler(Filters.text, do_echo)
        message_handler2 = CommandHandler('count', do_count)

        updater.dispatcher.add_handler(start_handler)
        updater.dispatcher.add_handler(help_handler)
        updater.dispatcher.add_handler(message_handler)
        updater.dispatcher.add_handler(buttons_handler)
        updater.dispatcher.add_handler(message_handler2)

        # 3 - start endless message processing
        updater.start_polling()
        updater.idle()
