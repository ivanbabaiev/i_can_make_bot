import requests

from logging import getLogger


logger = getLogger(__name__)

class HoroscopError(Exception):
    """
    Unknown error while requesting the Horoscope API
    """


class HoroscopRequestError(HoroscopError):
    """
    Error on invalid request
    """


class HoroscopAPI(object):
    """
    Client to API Horoscop
    Help on: https://github.com/tapaswenipathak/Horoscope-API
    """
    def __init__(self):
        self.base_url = "http://horoscope-api.herokuapp.com/horoscope/"

    def __request(self, period=None, sing=None):
        url = self.base_url + period + "/" + sing
        try:
            r = requests.get(url=url)
            result =  r.json()
        except Exception:
            logger.exception("Horoscop error")
            raise HoroscopError

        # if result.get("horoscope" or "sunsign" or "week"):
        #     # Successful request
        #     return result["sunsign", "week", "horoscope"]
        if result:
            # Successful request
            return result
        else:
            # Invalid request
            logger.error("Request error: %s", result.get("message"))
            raise HoroscopRequestError

    def get_last_text(self, period, sing):
        res = self.__request(period=period, sing=sing)
        return res

