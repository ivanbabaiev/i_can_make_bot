Django==3.1.4
python-telegram-bot==13.1
psycopg2==2.8.6
dj_database_url==0.5.0
gunicorn==20.0.4
whitenoise==5.2.0
requests==2.25.1