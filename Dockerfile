FROM python:3.8

WORKDIR /usr/src/test_bot_telegram

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV DEBIAN_FRONTEND=noninteractive

ARG DJANGO_SETTINGS_MODULE=tg_admin.settings

# install dependencies
RUN pip3 install --upgrade pip
COPY ./requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .



# Django specific commands
#RUN python3 -m pip install -U mod_wsgi-metrics==1.1.1 \
#  --no-cache-dir -r requirements.txt \
#  && python3 manage.py collectstatic --noinput \
#  && python3 manage.py compilemessages

#RUN apt-get update
#RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
#RUN apt-get install -y make  libffi-dev libjpeg-dev libz-dev gcc && rm -rf /var/lib/apt/lists/*
#RUN apk update
#RUN apk add --no-cache postgresql-dev gcc python3-dev musl-dev
#RUN apt-get update && apt-get install -y gcc python3-dev musl-dev
#COPY ./requirements.txt /test_bot_telegram/requirements.txt
#RUN pip install -r /test_bot_telegram/requirements.txt
#RUN /bin/sh -c python manage.py bot
#RUN python3 manage.py bot
